import os
import shutil


def get_directory_size(directory):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(directory):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # Skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)
    return total_size


def clear_cache(directory):
    try:
        for item in os.listdir(directory):
            item_path = os.path.join(directory, item)
            if os.path.isdir(item_path):
                shutil.rmtree(item_path)
                print(f"Cleared directory: {item_path}")
            else:
                os.remove(item_path)
                print(f"Cleared file: {item_path}")
    except Exception as e:
        print(f"Error: {e}")


def main():
    # Define the cache directory
    cache_directory = os.path.expanduser('~/Library/Caches')

    # Confirm the action with the user
    confirm = input(
        f"This will clear all cache files in {cache_directory}. Are you sure? (y/n): ")
    if confirm.lower() == 'y':
        # Get the size before clearing
        initial_size = get_directory_size(cache_directory)

        # Clear the cache
        clear_cache(cache_directory)

        # Get the size after clearing
        final_size = get_directory_size(cache_directory)

        # Calculate the cleared size
        cleared_size = initial_size - final_size
        print(f"Cleared {cleared_size / (1024 * 1024):.2f} MB of cache.")
    else:
        print("Operation cancelled.")


if __name__ == "__main__":
    main()
